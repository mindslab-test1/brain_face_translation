# -*- coding: utf-8 -*-
import os
from concurrent import futures

import grpc
import time

import chunk_pb2, chunk_pb2_grpc

CHUNK_SIZE = 1024 * 1024  # 1MB


def get_file_chunks(filename):
    with open(filename, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE);
            if len(piece) == 0:
                return
            yield chunk_pb2.Chunk(buffer=piece)


def save_chunks_to_file(chunks, filename):
    print("Saved file in client_tmp -> " + filename)
    with open(filename, 'wb') as f:
        for chunk in chunks:
            f.write(chunk.buffer)


class FileClient:
    def __init__(self, address):
        channel = grpc.insecure_channel(address)
        self.stub = chunk_pb2_grpc.FileServerStub(channel)

    def upload(self, in_file_name):
        chunks_generator = get_file_chunks(in_file_name)
        response = self.stub.upload(chunks_generator)
        return_response = str(response)[7:-2]
        return return_response

    def download(self, target_name, out_file_name):
        response = self.stub.download(chunk_pb2.Request(name=target_name))
        save_chunks_to_file(response, out_file_name)

    def download_2args(self, firstname, secondname, savefilename):
        response = self.stub.download_2args(chunk_pb2.Request_2args(name1=firstname,name2=secondname))
        save_chunks_to_file(response, savefilename)

    def avatar_processing(self, firstname, secondname, savename):
        if("None" in firstname):
            firstname = "source_img/id_000/center.png"
        if("None" in secondname):
            secondname = "source_driving/sample.mp4"
        self.download_2args(firstname, secondname, savename)




if __name__ == '__main__':
    if not (os.path.isdir("tmp")):
        os.makedirs(os.path.join("tmp"))
    client = FileClient('localhost:1446')

    # demo for file uploading
    in_file_name = '/home/won/Documents/Workspace/center.png'
    img_file_name = client.upload(in_file_name)
    print(img_file_name)

    in_file_name = '/home/won/Documents/Workspace/aaa.mp4'
    video_file_name = client.upload(in_file_name)
    print(video_file_name)

    client.avatar_processing(img_file_name, video_file_name, "./tmp/downloadfile.mp4")
