import numpy as np
import PIL.Image
from skimage.transform import resize

import face_alignment.api as face_alignment


def _set_midpoint(landmark_data):
    jaw_mid = (landmark_data[0] + landmark_data[16]) // 2
    # return (landmark_data[33] + jaw_mid) // 2
    return jaw_mid


def _get_ratio(landmark_data):
    jaw_vector = landmark_data[16] - landmark_data[0]
    jaw_dist = np.linalg.norm(jaw_vector)
    return jaw_dist, jaw_vector[0]

def crop_shortage(img, bbox, axis='width'):
    if axis == 'width':
        return max(0 - bbox[0], bbox[2] - img.size[0])
    elif axis == 'height':
        return max(0 - bbox[1], bbox[3] - img.size[1])
    else:
        raise AssertionError


class LandmarkDetector:
    def __init__(self, device, size=256):
        self.fa = face_alignment.FaceAlignment(face_alignment.LandmarksType._2D, flip_input=True, device=device)
        self.size = size
        self.y_center = -1000
        self.x_center = -1000
        self.ratio = 1.0
        self.frame = None
        self.jaw_ratio = dict()
        self.mode = ""
        self.crop_half = 0

    def reset_param(self):
        self.y_center = -1000
        self.x_center = -1000
        self.ratio = 1.0
        self.frame = None
        self.jaw_ratio = dict()


    def get_facial_landmark(self, image, ratio=0.7, mode='source'):
        """
        crop the face in any image with pre-defined ratio
        :param image: ndarray (H x W x 3(RGB))
        :param ratio: the proportion of face in an image (default: 0.7)
        :param mode: 'source' or 'driving' (default: 'source')
        :return: face-cropped ndarray
        """

        if self.mode != mode:
            self.reset_param()
            self.mode = mode

        if self.mode == 'source':
            return self._source_crop(image, ratio)
        else:
            return self._driving_crop(image, ratio)

    def _source_crop(self, image, ratio):

        self.real_source = image
        # 1. Max Square 잡기 (based on midpoint)
        kp_source = self.fa.get_landmarks(image)[0]
        self.midpoint = _set_midpoint(kp_source)
        _, jaw_x = _get_ratio(kp_source)

        mid_x, mid_y = self.midpoint[0], self.midpoint[1]
        img_x, img_y = image.shape[1], image.shape[0]
        margin = [abs(mid_y - 0), abs(mid_y - img_y), abs(mid_x - 0), abs(mid_x - img_x)]
        self.crop_half = int(min(margin))

        image_pil = PIL.Image.fromarray(image, mode='RGB')
        # 2. Ratio 적용했을 때 crop한 사이즈가 256 x 256 이상인지 확인. IF 이상이면, crop 진행
        if jaw_x / ratio >= 256:  # size check
            self.crop_half = min(int((jaw_x / ratio) / 2.0), self.crop_half)
            bbox = [int(self.midpoint[0] - self.crop_half),
                    int(self.midpoint[1] - self.crop_half),
                    int(self.midpoint[0] + self.crop_half),
                    int(self.midpoint[1] + self.crop_half)]
            img_cropped = image_pil.crop(bbox)

        # 2-1. IF 이하일 경우, crop 없이 256 이상인지 확인. IF 이상이면, simple resize
        elif self.crop_half * 2 >= 256:  # size check
            bbox = [int(self.midpoint[0] - self.crop_half),
                    int(self.midpoint[1] - self.crop_half),
                    int(self.midpoint[0] + self.crop_half),
                    int(self.midpoint[1] + self.crop_half)]
            img_cropped = image_pil.crop(bbox)
        # 2-2. IF 이하이면, bicubic interpolation to 256 x 256
        else:
            bbox = [int(self.midpoint[0] - self.crop_half),
                    int(self.midpoint[1] - self.crop_half),
                    int(self.midpoint[0] + self.crop_half),
                    int(self.midpoint[1] + self.crop_half)]
            img_cropped = image_pil.crop(bbox)

        return resize(np.array(img_cropped), (256, 256)), kp_source


    def restore(self, predictions, mode='source'):
        real_predictions = []
        self.midpoint = self.midpoint.astype(np.uint32)
        for frame in predictions:
            interp_frame = resize(frame, (self.crop_half * 2, self.crop_half * 2))
            real_frame = self.real_source / 255.0
            real_frame[self.midpoint[1]-self.crop_half+5:self.midpoint[1]+self.crop_half-5,
            self.midpoint[0]-self.crop_half+5:self.midpoint[0]+self.crop_half-5] = interp_frame[5:self.crop_half * 2-5, 5:self.crop_half * 2-5]
            real_predictions.append(real_frame)

        return real_predictions


    def _driving_crop(self, image, ratio):
        # if image.shape[0] < self.size * 1.1 and image.shape[1] < self.size * 1.1:
        #     return resize(image, (256, 256))

        self.jaw_ratio = {'min': ratio - 0.1, 'max': ratio + 0.1}
        image_pil = PIL.Image.fromarray(image, mode='RGB')
        kp_drive = self.fa.get_landmarks(np.array(image_pil))[0]

        self._update(kp_drive)
        frame = self._resize_crop_clip(image_pil)
        if frame.shape[0] != self.size or frame.shape[1] != self.size:
            return self.frame, kp_drive
        else:
            self.frame = frame
            return self.frame, kp_drive

    def _update(self, kp_drive):
        midpoint = _set_midpoint(kp_drive)
        jaw_dist, jaw_x = _get_ratio(kp_drive)
        if self.y_center is None or self.x_center is None or \
                abs(midpoint[0] - self.x_center) > 50 or \
                abs(midpoint[1] - self.y_center) > 50:
            self.x_center = midpoint[0]
            self.y_center = midpoint[1]
        # print(jaw_x, self.jaw_ratio * self.size, abs(jaw_z))
        # if jaw_x < self.jaw_ratio * self.size and abs(jaw_z) < 0.3:
        if (jaw_x * self.ratio < self.jaw_ratio['min'] * self.size or jaw_x * self.ratio > self.jaw_ratio['max'] * self.size):
            self.ratio = (self.jaw_ratio['min'] + self.jaw_ratio['max']) / 2 * self.size / jaw_dist

    def _resize_crop_clip(self, img):
        if isinstance(img, PIL.Image.Image):
            new_size = [int(x * self.ratio) for x in img.size]
            img = img.resize(new_size, resample=PIL.Image.BICUBIC)
            self.x_center_temp = self.x_center * self.ratio
            self.y_center_temp = self.y_center * self.ratio
            img = img.crop((int(self.x_center_temp - self.size / 2),
                            int(self.y_center_temp - self.size / 2),
                            int(self.x_center_temp + self.size / 2),
                            int(self.y_center_temp + self.size / 2)))
        else:
            raise TypeError('Expected numpy.ndarray or PIL.Image' +
                            'but got list of {0}'.format(type(img)))
        return np.array(img)
