import errno
import os
from random import randint
import matplotlib

from demo import load_checkpoints, make_animation, find_best_frame

matplotlib.use('Agg')

import os, sys
import yaml
from argparse import ArgumentParser
from tqdm import tqdm

import imageio
import numpy as np
from skimage.transform import resize
from skimage import img_as_ubyte
import torch
from scipy.spatial import ConvexHull

from sync_batchnorm import DataParallelWithCallback
from modules.generator import OcclusionAwareGenerator
from modules.keypoint_detector import KPDetector
from animate import normalize_kp
from util.preprocess import LandmarkDetector

import face_alignment.api as api
from random import randint
import PIL.Image as Image
import subprocess

from flask import Flask, render_template, request, url_for, send_file
from werkzeug.utils import secure_filename, redirect
import time
import atexit

from apscheduler.schedulers.background import BackgroundScheduler


app = Flask(__name__)


def makeOutput(srcid, dvid):
    config = "config/vox-adv-256.yaml"
    checkpoint = "pretrained/vox-adv-cpk.pth.tar"
    source_id = srcid
    driving_video = "./source_driving/" + dvid
    result_video = "./source_output/" + str(randint(1, 10000000)) + ".mp4"
    best_frame = None

    device_id = 'cuda'

    landmark_detector = LandmarkDetector(device=device_id, size=256)

    relative = True
    adapt_scale = True
    find_best_frame_var = True
    source_image = os.path.join("source_img", "id_{:03d}".format(source_id), "center.png")

    # Turn on the model
    generator, kp_detector = load_checkpoints(config_path=config, checkpoint_path=checkpoint)
    # Now, waiting for request

    source_image = np.array(Image.open(source_image))
    reader = imageio.get_reader(driving_video)
    vid_meta = reader.get_meta_data()
    fps, source_size, n_frames, duration = vid_meta['fps'], vid_meta['source_size'], vid_meta['nframes'], vid_meta['duration']
    # fps=29.97, source_size=(W, H), n_frames=393, duration=13.12
    reader.close()

    if driving_video is None:
        raise FileNotFoundError("No video source are selected!")

    # TODO: Use the header for choose whether to resize

    wid_hei = (subprocess.check_output(
        "ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=nw=1 " + driving_video,
        shell=True))
    print(wid_hei)
    wid_hei = str(wid_hei).split("\\")
    width = wid_hei[0].replace("b'width=", "")
    height = wid_hei[1].replace("nheight=", "")
    if (int(width) * int(height) > 262144):
        towidth = 512
        toheight = int(int(height) / int(width) * 512)
        subprocess.call("ffmpeg -y -i "+driving_video+" -s " + str(towidth) + "x" + str(
            toheight) + " -b:v 512k -vcodec mpeg1video -acodec copy "+ driving_video.replace("source_driving/","source_driving/2ver"), shell=True)

    driving_video = driving_video.replace("source_driving/","source_driving/2ver")
    driving_video = imageio.mimread(driving_video, memtest=False)

    source_image, _ = landmark_detector.get_facial_landmark(source_image[..., :3], ratio=0.5, mode='source')

    no_face_idx = []
    kp_drivings = []
    for idx, im in tqdm(enumerate(driving_video)):
        try:
            driving_video[idx], kp_driving = landmark_detector.get_facial_landmark(im[..., :3], mode='driving')
            kp_drivings.append(kp_driving)
        except:
            no_face_idx.append(idx)

    # print(no_face_idx)
    driving_video = [i / 255.0 for j, i in enumerate(driving_video) if j not in no_face_idx]

    if find_best_frame_var or best_frame is not None:
        i = best_frame if best_frame is not None else find_best_frame(source_image, driving_video, device_id, kp_drivings)
        # print ("Best frame: " + str(i))
        driving_forward = driving_video[i:]
        driving_backward = driving_video[:(i + 1)][::-1]
        predictions_forward = make_animation(source_image, driving_forward, generator, kp_detector,
                                             relative=relative, adapt_movement_scale=adapt_scale)
        predictions_backward = make_animation(source_image, driving_backward, generator, kp_detector,
                                              relative=relative, adapt_movement_scale=adapt_scale)
        predictions = predictions_backward[::-1] + predictions_forward[1:]
    else:
        predictions = make_animation(source_image, driving_video, generator, kp_detector, relative=relative,
                                     adapt_movement_scale=adapt_scale)
    imageio.mimsave(result_video, [img_as_ubyte(frame) for frame in predictions], fps=fps)

    return result_video


def makeOutput_by_path(srcpath, videopath):
    print("makeoutputbypath 1")
    config = "config/vox-adv-256.yaml"
    checkpoint = "pretrained/vox-adv-cpk.pth.tar"
    source_id = srcpath
    driving_video = videopath
    result_video = "./source_output/" + str(randint(1, 10000000)) + ".mp4"
    best_frame = None

    print("makeoutputbypath 2")
    device_id = 'cuda'

    print("makeoutputbypath 21")
    landmark_detector = LandmarkDetector(device=device_id, size=256)

    print("makeoutputbypath 22")
    relative = True
    adapt_scale = True
    find_best_frame_var = True
    source_image = srcpath

    print("makeoutputbypath 3")
    # Turn on the model
    generator, kp_detector = load_checkpoints(config_path=config, checkpoint_path=checkpoint)
    # Now, waiting for request

    print("makeoutputbypath 31")
    source_image = np.array(Image.open(source_image))
    print("makeoutputbypath 32")
    os.rename(driving_video,driving_video + ".mp4")
    reader = imageio.get_reader(driving_video+".mp4")
    print("makeoutputbypath 33")
    vid_meta = reader.get_meta_data()
    print("makeoutputbypath 34")
    fps, source_size, n_frames, duration = vid_meta['fps'], vid_meta['source_size'], vid_meta['nframes'], vid_meta['duration']
    # # fps=29.97, source_size=(W, H), n_frames=393, duration=13.12
    reader.close()
    os.rename(driving_video + ".mp4",driving_video)

    print("makeoutputbypath 4")
    if driving_video is None:
        raise FileNotFoundError("No video source are selected!")

    # TODO: Use the header for choose whether to resize

    wid_hei = (subprocess.check_output(
        "ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=nw=1 " + driving_video,
        shell=True))
    print(wid_hei)
    wid_hei = str(wid_hei).split("\\")
    width = wid_hei[0].replace("b'width=", "")
    height = wid_hei[1].replace("nheight=", "")
    if (int(width) * int(height) > 262144):
        towidth = 512
        toheight = int(int(height) / int(width) * 512)
        subprocess.call("ffmpeg -y -i "+driving_video+" -s " + str(towidth) + "x" + str(
            toheight) + " -b:v 512k -vcodec mpeg1video -acodec copy "+ driving_video + "2ver.mp4", shell=True)

    print("makeoutputbypath 5")
    driving_video = driving_video + "2ver.mp4"
    driving_video = imageio.mimread(driving_video, memtest=False)

    source_image, _ = landmark_detector.get_facial_landmark(source_image[..., :3], ratio=0.5, mode='source')

    print("makeoutputbypath 6")
    no_face_idx = []
    kp_drivings = []
    for idx, im in tqdm(enumerate(driving_video)):
        try:
            driving_video[idx], kp_driving = landmark_detector.get_facial_landmark(im[..., :3], mode='driving')
            kp_drivings.append(kp_driving)
        except:
            no_face_idx.append(idx)

    # print(no_face_idx)
    driving_video = [i / 255.0 for j, i in enumerate(driving_video) if j not in no_face_idx]

    print("makeoutputbypath 7")
    if find_best_frame_var or best_frame is not None:
        i = best_frame if best_frame is not None else find_best_frame(source_image, driving_video, device_id, kp_drivings)
        # print ("Best frame: " + str(i))
        driving_forward = driving_video[i:]
        driving_backward = driving_video[:(i + 1)][::-1]
        predictions_forward = make_animation(source_image, driving_forward, generator, kp_detector,
                                             relative=relative, adapt_movement_scale=adapt_scale)
        predictions_backward = make_animation(source_image, driving_backward, generator, kp_detector,
                                              relative=relative, adapt_movement_scale=adapt_scale)
        predictions = predictions_backward[::-1] + predictions_forward[1:]
    else:
        predictions = make_animation(source_image, driving_video, generator, kp_detector, relative=relative,
                                     adapt_movement_scale=adapt_scale)
    imageio.mimsave(result_video, [img_as_ubyte(frame) for frame in predictions], fps=fps)

    print("makeoutputbypath 8")
    return result_video

@app.route('/', methods=['POST'])
def api_connect():
    f = open('./now_config/config_t', 'r')
    s = int(f.read())
    f.close()
    f = open('./now_config/config_t', 'w')
    f.write(str(s + 1))
    f.close()
    try:
        media = request.files['media']
        video_name = secure_filename(media.filename)
        media.save("source_driving/" + video_name)
    except:
        video_name = "sample.mp4"
    try:
        id_img_no = randint(100, 999)
        id_img = request.files['id_img']
        try:
            if not (os.path.isdir("source_img/id_" + str(id_img_no))):
                os.makedirs(os.path.join("source_img/id_" + str(id_img_no)))
        except OSError as e:
            if e.errno != errno.EEXIST:
                print("Failed to create directory!!!!!")
                raise
        id_img.save("source_img/id_" + str(id_img_no) + "/center.png")
    except:
        id_img_no = 0
    # if(id_img_no==0 and "sample.mp4" in video_name):
    #     print("Use sample video")
    #     outputname = "source_output/result.mp4"
    # else:
    #     outputname = makeOutput(id_img_no,video_name)
    outputname = makeOutput(id_img_no, video_name)
    f = open('./now_config/config_t', 'r')
    s = int(f.read())
    f.close()
    f = open('./now_config/config_t', 'w')
    f.write(str(s - 1))
    f.close()

    return send_file(outputname)


def print_date_time():
    del_list = list()
    # now = time.gmtime(time.time())
    # print(now.tm_hour)
    # print(now.tm_min)
    # print(now.tm_sec)
    path = "./source_driving"
    driving_list = os.listdir(path)
    for I in driving_list:
        if not ("sample" in I):
            del_list.append("./source_driving/" + I)
    path = "./source_img"
    src_img_list = os.listdir(path)
    for I in src_img_list:
        if not ("id_000" in I):
            del_list.append("./source_img/" + I)
    path = "./source_output"
    output_list = os.listdir(path)
    for I in output_list:
        if not ("result.mp4" in I):
            del_list.append("./source_output/" + I)

    f = open('./now_config/config_t', 'r')
    s = int(f.read())
    f.close()
    if (s == 0):
        print("no process")
        for i in del_list:
            if ("source_img" in i):
                src_list = os.listdir(i)
                os.remove(i + "/" + src_list[0])
                os.rmdir(i + "/")
            else:
                os.remove(i)
    # else:
    # print("now - process")
    # print(del_list)


if __name__ == '__main__':
    try:
        if not (os.path.isdir("source_driving")):
            os.makedirs(os.path.join("source_driving"))
        if not (os.path.isdir("source_img")):
            os.makedirs(os.path.join("source_img"))
            if not (os.path.isdir("source_output")):
                os.makedirs(os.path.join("source_output"))

        if not (os.path.isdir("now_config")):
            os.makedirs(os.path.join("now_config"))
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

    f = open('./now_config/config_t', 'w')
    f.write('0')
    f.close()
    scheduler = BackgroundScheduler()
    scheduler.add_job(func=print_date_time, trigger="interval", seconds=10)
    scheduler.start()
    app.run(host='0.0.0.0',port=1423)
    atexit.register(lambda: scheduler.shutdown())




