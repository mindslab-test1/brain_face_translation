# Avatar Packaging

## Version Compatiblity

- [ ] CUDA >10.0
- [ ] CuDNN 7.0
- [ ] python >3.6
- [ ] pip library는 [requirements.txt](./requirements.txt) 에 명시

## Pipeline Detail

### setting
`bash setup.sh`
[setup.sh](./setup.sh)
```
git clone https://github.com/1adrianb/face-alignment
cd face-alignment && python setup.py install && cd ..
mkdir pretrained && wget -O pretrained/vox-adv-cpk.pth.tar https://kyumaze.s3.ap-northeast-2.amazonaws.com/avatar/vox-adv-cpk.pth.tar
```

첫 실행에 있어서는 face_alignment에 필요한 weight 파일이 다운로드 됨.

### Input
- **Source Images** (움직이고 싶은 사진, 보기 중 선택)  
Source Image는 각 ID(사람)에 대해 서로 다른 view로 5장씩 마련되어 있다. 현재는 정면 사진 1가지만 사용하고 있다.
- **Driving Video** (User Video, `사이즈 무관, .mp4`)

### Output
- **Animated Video** (움직이는 Video, `256x256, .mp4`)

### Main Command
```
python demo.py --driving_video path/to/driving --source_id ID_NUM --result_video path/to/result
```

### User 제약 사항
- 얼굴 각도를 과하게 움직이지 않는 것을 권장. (정면 기준 20도 이내 권장)
- 얼굴이 전체 영상 중 9분의 1 면적 이상을 차지하는 것을 권장.
- 동영상 중에 무표정으로 정면을 보고 있는 장면이 있을 경우, 생성 영상의 품질이 훨씬 좋음.

### Model Checkpoint
- `pretrained/vox-adv-cpk.pth.tar` 에 저장되어 있음.


## Packaging Detail

### HTTP

### Flask

### Landing Page 구성
- Input Source Image들을 보여주어 선택할 수 있는 window
- Driving Video 넣을 수 있는 window
- [User 제약 사항](https://github.com/kyumaze/avatar-pkg#user-%EC%A0%9C%EC%95%BD-%EC%82%AC%ED%95%AD)

### Reference
- Fork from [First Order Motion Model](https://github.com/AliaksandrSiarohin/first-order-model)


