import matplotlib

matplotlib.use('Agg')

import os, sys
import yaml
from argparse import ArgumentParser
from tqdm import tqdm

import imageio
import numpy as np
from skimage.transform import resize
from skimage import img_as_ubyte
import torch
from scipy.spatial import ConvexHull

from sync_batchnorm import DataParallelWithCallback
from modules.generator import OcclusionAwareGenerator
from modules.keypoint_detector import KPDetector
from animate import normalize_kp
from util.preprocess import LandmarkDetector

import face_alignment.api as api
from random import randint
import PIL.Image as Image
import subprocess

if sys.version_info[0] < 3:
    raise Exception("You must use Python 3 or higher. Recommended version is Python 3.7")


def load_checkpoints(config_path, checkpoint_path, cpu=False):
    print(os.getcwd())
    with open(config_path) as f:
        config = yaml.load(f)

    generator = OcclusionAwareGenerator(**config['model_params']['generator_params'],
                                        **config['model_params']['common_params'])

    if not cpu:
        generator.cuda()

    kp_detector = KPDetector(**config['model_params']['kp_detector_params'],
                             **config['model_params']['common_params'])
    if not cpu:
        kp_detector.cuda()

    if cpu:
        checkpoint = torch.load(checkpoint_path, map_location=torch.device('cpu'))
    else:
        checkpoint = torch.load(checkpoint_path)

    generator.load_state_dict(checkpoint['generator'])
    kp_detector.load_state_dict(checkpoint['kp_detector'])

    if not cpu:
        generator = DataParallelWithCallback(generator)
        kp_detector = DataParallelWithCallback(kp_detector)

    generator.eval()
    kp_detector.eval()

    return generator, kp_detector


def make_animation(source_image, driving_video, generator, kp_detector, relative=True, adapt_movement_scale=True,
                   cpu=False):
    with torch.no_grad():
        predictions = []
        source = torch.tensor(source_image[np.newaxis].astype(np.float32)).permute(0, 3, 1, 2)
        if not cpu:
            source = source.cuda()
        driving = torch.tensor(np.array(driving_video)[np.newaxis].astype(np.float32)).permute(0, 4, 1, 2, 3)
        kp_source = kp_detector(source)
        kp_driving_initial = kp_detector(driving[:, :, 0])

        for frame_idx in tqdm(range(driving.shape[2])):
            driving_frame = driving[:, :, frame_idx]
            if not cpu:
                driving_frame = driving_frame.cuda()
            kp_driving = kp_detector(driving_frame)
            kp_norm = normalize_kp(kp_source=kp_source, kp_driving=kp_driving,
                                   kp_driving_initial=kp_driving_initial, use_relative_movement=relative,
                                   use_relative_jacobian=relative, adapt_movement_scale=adapt_movement_scale)
            out = generator(source, kp_source=kp_source, kp_driving=kp_norm)

            predictions.append(np.transpose(out['prediction'].data.cpu().numpy(), [0, 2, 3, 1])[0])
    return predictions


def find_best_frame(source, driving, device_id, kp_drivings):
    def normalize_kp(kp):
        kp = kp - kp.mean(axis=0, keepdims=True)
        area = ConvexHull(kp[:, :2]).volume
        area = np.sqrt(area)
        kp[:, :2] = kp[:, :2] / area
        return kp

    fa = api.FaceAlignment(api.LandmarksType._2D, flip_input=True,
                                      device=device_id)
    kp_source = fa.get_landmarks(255 * source)[0]
    kp_source = normalize_kp(kp_source)
    norm = float('inf')
    frame_num = 0
    for i, kp_driving in tqdm(enumerate(kp_drivings)):
        # kp_driving = fa.get_landmarks(255 * image)[0]
        kp_driving = normalize_kp(kp_driving)
        new_norm = (np.abs(kp_source - kp_driving) ** 2).sum()
        if new_norm < norm:
            norm = new_norm
            frame_num = i
    return frame_num

if __name__ == '__main__':
    config = "config/vox-adv-256.yaml"
    checkpoint = "pretrained/vox-adv-cpk.pth.tar"
    source_id = 0
    driving_video = "./source_driving/" + "sample.mp4"
    result_video = "./source_output/" + str(randint(1, 10000000)) + ".mp4"
    best_frame = None

    device_id = 'cuda'

    landmark_detector = LandmarkDetector(device=device_id, size=256)

    relative = True
    adapt_scale = True
    find_best_frame_var = True
    source_image = os.path.join("source_img", "id_{:03d}".format(source_id), "center.png")

    # Turn on the model
    generator, kp_detector = load_checkpoints(config_path=config, checkpoint_path=checkpoint)
    # Now, waiting for request

    source_image = np.array(Image.open(source_image))
    reader = imageio.get_reader(driving_video)
    vid_meta = reader.get_meta_data()
    fps, source_size, n_frames, duration = vid_meta['fps'], vid_meta['source_size'], vid_meta['nframes'], vid_meta['duration']
    # fps=29.97, source_size=(W, H), n_frames=393, duration=13.12
    reader.close()

    if driving_video is None:
        raise FileNotFoundError("No video source are selected!")

    # TODO: Use the header for choose whether to resize
    subprocess.call("", shell=True)

    driving_video = imageio.mimread(driving_video, memtest=False)

    print("Before: ", np.amax(source_image), np.amin(source_image))
    source_image, _ = landmark_detector.get_facial_landmark(source_image[..., :3], ratio=0.5, mode='source')
    print("After: ", np.amax(source_image), np.amin(source_image))
    imageio.imsave("cropped_source.png", source_image)
    # source_image: (float64) [0.0, 1.0]

    # distill frames which does not contain face image
    no_face_idx = []
    kp_drivings = []
    for idx, im in tqdm(enumerate(driving_video)):
        try:
            driving_video[idx], kp_driving = landmark_detector.get_facial_landmark(im[..., :3], mode='driving')
            kp_drivings.append(kp_driving)
        except:
            no_face_idx.append(idx)

    driving_video = [i / 255.0 for j, i in enumerate(driving_video) if j not in no_face_idx]
    assert len(kp_drivings) == len(driving_video)
    # source_image: (float64) [0.0, 1.0]
    imageio.mimsave("driving.mp4", [img_as_ubyte(frame) for frame in driving_video], fps=fps)

    if find_best_frame_var or best_frame is not None:
        i = best_frame if best_frame is not None else find_best_frame(source_image, driving_video, device_id, kp_drivings)
        # print ("Best frame: " + str(i))
        driving_forward = driving_video[i:]
        driving_backward = driving_video[:(i + 1)][::-1]
        predictions_forward = make_animation(source_image, driving_forward, generator, kp_detector,
                                             relative=relative, adapt_movement_scale=adapt_scale)
        predictions_backward = make_animation(source_image, driving_backward, generator, kp_detector,
                                              relative=relative, adapt_movement_scale=adapt_scale)
        predictions = predictions_backward[::-1] + predictions_forward[1:]
    else:
        predictions = make_animation(source_image, driving_video, generator, kp_detector, relative=relative,
                                     adapt_movement_scale=adapt_scale)

    # predictions: (float64) [0.0, 1.0]
    imageio.mimsave(result_video, [img_as_ubyte(frame) for frame in predictions], fps=fps)
    # real_predictions = landmark_detector.restore(predictions)
    # imageio.mimsave(result_video[:-4] + "_up.mp4", [img_as_ubyte(frame) for frame in real_predictions], fps=fps)
    print(result_video, "is saved.")