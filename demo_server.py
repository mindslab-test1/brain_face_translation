# -*- coding: utf-8 -*-
import os
import random
import shutil
from concurrent import futures


import grpc
import time

import api_server
import chunk_pb2, chunk_pb2_grpc

CHUNK_SIZE = 1024 * 1024  # 1MB

def get_file_chunks(filename):
    with open(filename, 'rb') as f:
        while True:
            piece = f.read(CHUNK_SIZE);
            if len(piece) == 0:
                return
            yield chunk_pb2.Chunk(buffer=piece)


def save_chunks_to_file(chunks, filename):
    print("save chunks to file name : " + filename)
    with open(filename, 'wb') as f:
        for chunk in chunks:
            f.write(chunk.buffer)

class FileServer(chunk_pb2_grpc.FileServerServicer):
    def __init__(self):

        class Servicer(chunk_pb2_grpc.FileServerServicer):
            def __init__(self):
                self.tmp_file_name = '/tmp/server_tmp'

            def upload(self, request_iterator, context):
                if not (os.path.isdir("tmp")):
                    os.makedirs(os.path.join("tmp"))
                if not (os.path.isdir("tmp/server_tmp")):
                        os.makedirs(os.path.join("tmp/server_tmp"))


                self.tmp_file_name = './tmp/server_tmp/' + str(random.randrange(1,10000000))
                save_chunks_to_file(request_iterator, self.tmp_file_name)
                return chunk_pb2.Request(name=self.tmp_file_name)

            def download(self, request, context):
                if not (os.path.isdir("tmp")):
                    os.makedirs(os.path.join("tmp"))
                if not (os.path.isdir("tmp/server_tmp")):
                        os.makedirs(os.path.join("tmp/server_tmp"))

                if request.name:
                    return get_file_chunks(self.tmp_file_name)

            def download_2args(self, request, context):
                if not (os.path.isdir("tmp")):
                    os.makedirs(os.path.join("tmp"))
                if not (os.path.isdir("tmp/server_tmp")):
                        os.makedirs(os.path.join("tmp/server_tmp"))


                tmp_ret_file_path = api_server.makeOutput_by_path(request.name1, request.name2)

                if request.name1:
                    return get_file_chunks(tmp_ret_file_path)

        self.server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
        chunk_pb2_grpc.add_FileServerServicer_to_server(Servicer(), self.server)

    def start(self, port):
        self.server.add_insecure_port(f'[::]:{port}')
        self.server.start()

        try:
            while True:
                time.sleep(600)
                if (os.path.isdir("tmp")):
                    shutil.rmtree('tmp')
        except KeyboardInterrupt:
            self.server.stop(0)


if __name__ == '__main__':
    print("server on")
    FileServer().start(1446)
